

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),

      initialRoute: '/',
      routes: {
        '/':(context)=>FirstPage(),
        '/SecondPage':(context)=>SecondPage(),

        //这样定义路由在用的时候不会写作,用点语法即可.
//        '/ThirdPage':(context)=>ThirdPage(),
        ThirdPage.routeName:(context)=>ThirdPage(),
        FourthPage.routeName:(context)=>FourthPage(),
        FifthPage.routeName:(context)=>FifthPage(),
      },

    );


  }
}


//模型类 用来存储传递的参数
class ScreenArguments{
  final String title;
  final String message;

  //构造方法(传参相当于初始化)
  ScreenArguments(this.title,this.message);
}

//第一个页面
class FirstPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('firstPage'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(onPressed: (){
              Navigator.pushNamed(context, '/SecondPage');
            },
              child: Text('顺传演示'),
            ),
            RaisedButton(onPressed: (){
              Navigator.pushNamed(
                context,
                FourthPage.routeName,
              );
            },
              child: Text('逆传演示'),
            ),
          ],
        ),
      ),
    );
  }
}

//第二个页面
class SecondPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('secondPage'),
      ),
      body: Center(
        child: RaisedButton(
          onPressed: (){
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => ThirdPage(
                      argus:ScreenArguments(
                          'qwer',
                          'MESSAGE'
                      )
                  ),
//                settings: RouteSettings(
//
//                )
              //true 弹出效果相当于modal
              fullscreenDialog: true,
              ));
        },
          child: Text('click to jump'),
        ),
      ),
    );
  }
}




//第三个页面
class ThirdPage extends StatelessWidget {
  ThirdPage({Key key ,this.argus}):super(key:key);

  //定义路由,外面直接用,不用担心写错.
  static const routeName = '/extractArguments';
  final ScreenArguments argus ;


  @override
  Widget build(BuildContext context) {
//    final ScreenArguments args = ModalRoute.of(context).settings.name;
    print('$ModalRoute');
    return Scaffold(
      appBar: AppBar(
        title: Text(argus.title),
      ),
      body: Center(
        child: RaisedButton(onPressed: (){
          Navigator.pop(context);
        },
          child: Text('click to jump '),
        ),
      ),
    );
  }
}

/*
*页面跳转传参的步骤 (行不通了,路由settings中没有arguments的参数了)
1.Define the arguments you need to pass(定义一个参数传递的模型类)
2.Create a widget that extracts the arguments(定义使用(解析)参数的接收类)
3.Register the widget in the routes table(路由表中注册这个接收类)
4.Navigate to the widget(跳转(导航)到这个页面)
5.extract the arguments using onGenerateRoute(另外,用onGenerateRoute解析)
* */



/*四五两个页面演示逆向传值*/

class FourthPage extends StatefulWidget {
  //第四个页面
  FourthPage({Key key ,this.argus}):super(key:key);
  //定义路由,外面直接用,不用担心写错.
  static const routeName = '/FourthPage';
  final ScreenArguments argus ;
  @override
  _FourthPageState createState() => _FourthPageState();
}

class _FourthPageState extends State<FourthPage> {
  var string = '显示回传值';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('第四个页面'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              color: Colors.blue,
              child:Text('$string'),
            ),
            buildRaisedButton(context),
          ],
        ),
      ),
    );
  }

  RaisedButton buildRaisedButton(BuildContext context) {
    return RaisedButton(onPressed: (){
          _clickToPush(context);
          },
            child: Text('click to jump '),
          );
  }

  _clickToPush(BuildContext context)async{
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context)=>FifthPage()),
    );


    print('---返回值是:$result');

    setState(() {
      string = result;
    });

  }


}

//第五个页面
class FifthPage extends StatelessWidget {
  FifthPage({Key key ,this.argus}):super(key:key);

  //定义路由,外面直接用,不用担心写错.
  static const routeName = '/FifthPage';
  final ScreenArguments argus ;


  @override
  Widget build(BuildContext context) {
//    final ScreenArguments args = ModalRoute.of(context).settings.name;
    print('$ModalRoute');
    return Scaffold(
      appBar: AppBar(
        title: Text('第五个页面'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment:MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(onPressed: (){
              Navigator.pop(context,'回传一');
            },
              child: Text('回传一'),
            ),
            RaisedButton(onPressed: (){
              Navigator.pop(context,'回传二');
            },
              child: Text('回传二'),
            ),
          ],
        ),
      ),
    );
  }
}


